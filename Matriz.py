def Opt():
    print('0 - Para parar')
    print('1 - Exercício 1')
    print('2 - Exercício 2')
    print('3 - Exercício 3')
    s = int(input())
    print('')
    return s

def Opt1():
    print('0 - Para parar')
    print('1 - Para gerar uma matriz diagonal')
    print('2 - Para gerar uma matriz identidade')
    print('3 - Para gerar uma matriz simétrica')
    print('4 - Para gerar uma matriz transposta')
    print('5 - Para Somar as matrizes')
    print('6 - Para Subtrair as matrizes')
    print('7 - Para multiplicar com uma constante')
    print('8 - Para multiplicar as Matrizes')
    s = int(input())
    print('')
    return s

def Opt2():
    print('0 - Para parar')
    print('1 - Determinante 2x2')
    print('2 - Determinante 3x3')
    print('3 - Determinante 4x4')
    s = int(input())
    print('')
    return s

def Opt3():
    print('0 - Para parar')
    print('1 - Determinante do Sistema Linear de ordem 2')
    print('2 - Determinante do Sistema Linear de ordem 3')
    s = int(input())
    print('')
    return s 

def Mostrar(matriz, coluna, linha):
    for a in range(linha):
        for b in range(coluna):
            print("{}\t".format(matriz[a][b]), end ='')
        print('')
    print('')
    return

def MatrizInsert(matriz, coluna, linha):
    for x in range(linha):
        for y in range(coluna):
            matriz[x][y] = int(input('Insira um valor da\nlinha {} coluna {} : '.format(x+1, y+1)))
    return matriz

def CMatriz(coluna, linha):
    matriz = [[0 for x in range(coluna)] for y in range(linha)]
    return matriz

def Det2x2(matriz):
    igual = 1
    diferente = 1
    for i in range(2):
        for j in range(2):
            if i == j:
                igual = igual * matriz[i][j]
            else:
                diferente = diferente * matriz[i][j]
    return igual - diferente

def Det3x3(matriz):
    p = 0
    s = 0
    a = 1
    b = 2
    for j in range(3):
        if a > 2:
            a = 0
        if b > 2:
            b = 0
        p = matriz[0][j] * matriz[1][a] * matriz[2][b] + p
        s = matriz[2][j] * matriz[1][a] * matriz[0][b] + s
        a = a + 1
        b = b + 1
    return (p-s)

def DetAux3x3(matriz, exlinha, excoluna):
    aux = CMatriz(3, 3)
    for i in range(4):
        if i != exlinha:
            for j in range(4):
                if j != excoluna:
                    if i > exlinha:
                        if j > excoluna:
                            aux[i-1][j-1] = matriz[i][j]
                        else:
                            aux[i-1][j] = matriz[i][j]
                    else:
                        if j > excoluna:
                            aux[i][j-1] = matriz[i][j]
                        else:
                            aux[i][j] = matriz[i][j]
    return Det3x3(aux)

s = Opt()

while s != 0:
    if s == 1:
        s2 = Opt1()
        while s2 != 0:
            if s2 == 1 or s2 == 2 or s2 == 3:
                o = int(input('Insira a ordem: '))
                matriz = CMatriz(o, o)
                
                if s2 == 1:
                    print('\n----- Matriz Diagonal -----\n\n')
                    for i in range(o):
                        for j in range(o):
                            if i == j:
                                matriz[i][j] = int(input('Insira um número: '))
                            else:
                                matriz[i][j] = 0
                    print('')
                    Mostrar(matriz, o, o)

                if s2 == 2:
                    print('\n----- Matriz Identidade -----\n\n')
                    for i in range(o):
                        for j in range(o):
                            if i == j:
                                matriz[i][j] = 1
                            else:
                                matriz[i][j] = 0

                    Mostrar(matriz, o, o)

                if s2 == 3:
                    print('\n----- Matriz Simétrica -----\n\n')
                    ss = 1
                    for i in range(o):
                        for j in range(o):
                            if i != j:
                                if matriz[i][j] == 0 and matriz[j][i] == 0:
                                    matriz[i][j] = ss
                                    matriz[j][i] = ss
                                    ss = ss + 1

                    Mostrar(matriz, o, o)

            if s2 == 4:
                print('\n----- Matriz Transposta ------\n\n')
                linha = int(input('Insira quantas linhas voce quer: '))
                coluna = int(input('Insira quantas colunas voce quer:'))
                matriz = CMatriz(coluna, linha) 

                matriz = MatrizInsert(matriz, coluna, linha)
                
                transposta = CMatriz(linha, coluna)

                Mostrar(matriz, coluna, linha)
                
                for x in range(linha):
                    for y in range(coluna):
                        transposta[y][x] = matriz[x][y]

                Mostrar(transposta,linha,coluna)

            if s2 == 5:
                print('\n----- Somar as matrizes ------\n\n')
                linhaA = int(input('Insira quantas linhas para A: '))
                colunaA = int(input('Insira quantas colunas para A: '))
                linhaB = int(input('Inserir quantas linhas para B: '))
                colunaB = int(input('Inserir quantas colunas para B: '))
                if linhaA == linhaB and colunaA == colunaB:
                    matrizA = CMatriz(colunaA, linhaA)
                    matrizB = CMatriz(colunaB, linhaB)
                    print('\n-----  MatrizA  -----\n')
                    matrizA = MatrizInsert(matrizA, colunaA, linhaA)
                    Mostrar(matrizA, colunaA, linhaA)
                    print('\n-----  MatrizB  -----\n')
                    matrizB = MatrizInsert(matrizB, colunaA, linhaA)
                    Mostrar(matrizB, colunaA, linhaA)
                    soma = CMatriz(colunaA, linhaA)
                    for x in range(linhaA):
                        for y in range(colunaA):
                            soma[x][y] = matrizA[x][y] + matrizB[x][y]
                    print('\n----- RESULTADO da Soma -----\n{}\n\n'.format(Mostrar(soma, colunaA, linhaA)))
            
            if s2 == 6:
                print('\n----- Subtrair as matrizes ------\n\n')
                linhaA = int(input('Insira quantas linhas para A: '))
                colunaA = int(input('Insira quantas colunas para A: '))
                linhaB = int(input('Inserir quantas linhas para B: '))
                colunaB = int(input('Inserir quantas colunas para B: '))
                if linhaA == linhaB and colunaA == colunaB:
                    matrizA = CMatriz(colunaA, linhaA)
                    matrizB = CMatriz(colunaB, linhaB)
                    print('\n-----  MatrizA  -----\n')
                    matrizA = MatrizInsert(matrizA, colunaA, linhaA)
                    Mostrar(matrizA, colunaA, linhaA)
                    print('\n-----  MatrizB  -----\n')
                    matrizB = MatrizInsert(matrizB, colunaA, linhaA)
                    Mostrar(matrizB, colunaA, linhaA)
                    sub = CMatriz(colunaA, linhaA)
                    for x in range(linhaA):
                        for y in range(colunaA):
                            sub[x][y] = matrizA[x][y] - matrizB[x][y]
                    Mostrar(sub, colunaA, linhaA)

            if s2 == 7:
                print('\n----- multiplicar com uma constante ------\n\n')
                cnst = int(input('Insira a constante: '))
                linha = int(input('Insira quantas linhas: '))
                coluna = int(input('Insira quantas colunas: '))

                matriz = CMatriz(coluna, linha)
                matriz = MatrizInsert(matriz, coluna, linha)
                Mostrar(matriz, coluna, linha)

                for x in range(linha):
                    for y in range(coluna):
                        matriz[x][y] = matriz[x][y] * cnst

                Mostrar(matriz, coluna, linha)

            if s2 == 8:
                print('\n----- multiplicar as Matrizes SEM Constante ------\n\n')
                linhaA = int(input('Insira o numero de linhas da matrizA: '))
                colunaA = int(input('Insira o numero de colunas da matrizA: '))
                linhaB = int(input('Insira o numero de linhas da MatrizB:  '))
                colunaB = int(input('Insira o numero de colunas da matrizB: '))
                if colunaA == linhaB:
                    matrizA = CMatriz(colunaA, linhaA)
                    matrizB = CMatriz(colunaB, linhaB)
                    soma = CMatriz(colunaB, linhaA)
                    
                    print('\n----  Inserir MatrizA  ----\n')
                    MatrizInsert(matrizA, colunaA, linhaA)
                    Mostrar(matrizA, colunaA, linhaA)
                    print('\n----  Inserir MatrizB  ----\n')
                    MatrizInsert(matrizB, colunaB, linhaB)
                    Mostrar(matrizB, colunaB, linhaB)
                    s = 0
                    for i in range(linhaA):
                        for j in range(colunaB):
                            for k in range(colunaA):
                                s = (matrizA[i][k] * matrizB[k][j]) + s
                            soma[i][j] = s

                    Mostrar(soma, colunaA, linhaA)

            s2 = Opt1()
    
    if s == 2:
        s2 = Opt2()
        while s2 != 0:
            if s2 == 1:
                print("----- Determinante 2x2 -----\n\n")
                matriz = CMatriz(2,2)
                matriz = MatrizInsert(matriz, 2, 2)
                Mostrar(matriz, 2, 2)
        
                print('\n ----- RESULTADO da determinante 2x2 ------\n{}\n\n'.format(Det2x2(matriz)))

            if s2 == 2:    
                print("----- Determinante 3x3 -----\n\n")

                matriz = CMatriz(3,3)
                matriz = MatrizInsert(matriz, 3, 3)
                Mostrar(matriz, 3, 3)

                conta = Det3x3(matriz)

                print("\n ----- RESULTADO da determinante 3x3 -----\n{}\n\n".format(conta))

            if s2 == 3:    
                print("----- Determinante 4x4 -----\n\n")
                
                matriz = CMatriz(4, 4)
                matriz = MatrizInsert(matriz, 4, 4)
                Mostrar(matriz, 4, 4)
                conta = 0

                exlinha = int(input("Insira qual linha deseja retirar: ")) - 1

                for j in range(4):
                    conta = matriz[exlinha][j]*pow(-1,exlinha+j+2)*DetAux3x3(matriz,exlinha, j) + conta

                print("\n----- RESULTADO Determinante 4x4 -----\n{}\n\n".format(conta))

            s2 = Opt2()
    
    if s == 3:
        s2 = Opt3()
        while s2 != 0:
            if s2 == 1:
                print('----- Determinante do Sistema Linear de ordem 2 -----\n\n')
                
                matriz = CMatriz(3, 2)

                for i in range(2):
                    for j in range(3):
                        if j < 2:
                            matriz[i][j] = int(input('Insira A{}{}: '.format(i+1,j+1)))
                        elif j == 2:
                            matriz[i][j] = int(input('Insira B{} '.format(i+1)))
                print('')
                for i in range(2):
                    print('{}x + {}y = {}'.format(matriz[i][0],matriz[i][1],matriz[i][2]))
                print('')
                Mostrar(matriz, 2, 2)

                ax = CMatriz(2,2)
                ay = CMatriz(2,2)
                determinante = Det2x2(matriz)

                for i in range(2):
                    for j in range(3):
                        if j == 0:
                            ax[i][j] = matriz[i][j+2]
                            ay[i][j] = matriz[i][j]
                        if j == 1: 
                            ax[i][j] = matriz[i][j]
                            ay[i][j] = matriz[i][j+1]
                
                detX = Det2x2(ax)
                detY = Det2x2(ay)
                
                if determinante != 0:
                    if detX == 0:
                        x = 0
                    if detY == 0:
                        y = 0
                    if x != 0:
                        x = detX/determinante
                    if y != 0:
                        y = detY/determinante

                    Mostrar(ax, 2, 2)
                    print('')
                    Mostrar(ay, 2, 2)
                    print('\nDeterminante = {}\nDetX = {}\nDetY = {}\n'.format(determinante,detX,detY))
                    print('\n----- RESULTADO SPD Sistema Linear 2x2 ------\nDeterminante {}\nX = {}\nY = {}\n\n'.format(determinante,x,y))
                else:
                    if detX == 0 and detY == 0:
                        print('\n----- SPL -----\n\n')
                    if detX != 0 or detY != 0:
                        print('\n----- SI -----\n\n') 

            if s2 == 2:
                print("----- Determinante do Sistema Linear de ordem 3 -----\n\n")
                
                matriz = CMatriz(4,3)

                for i in range(3):
                    for j in range(4):
                        if j < 3:
                            matriz[i][j] = int(input('Insira A{}{}: '.format(i+1,j+1)))
                        elif j == 3:
                            matriz[i][j] = int(input('Insira B{} '.format(i+1)))
                print('')
                for i in range(3):
                    print('{}x + {}y + {}z = {}'.format(matriz[i][0],matriz[i][1],matriz[i][2], matriz[i][3]))
                print('')
                Mostrar(matriz, 3, 3)

                ax = CMatriz(3,3)
                ay = CMatriz(3,3)
                az = CMatriz(3,3)
                determinante = Det3x3(matriz)

                for i in range(3):
                    for j in range(4):
                        if j < 3:
                            if j == 0:
                                ax[i][j] = matriz[i][j+3]
                                ay[i][j] = matriz[i][j]
                                az[i][j] = matriz[i][j]
                            if j == 1:
                                ay[i][j] = matriz[i][j+2]
                                ax[i][j] = matriz[i][j]
                                az[i][j] = matriz[i][j]
                            if j == 2:
                                az[i][j] = matriz[i][j+1]
                                ax[i][j] = matriz[i][j]
                                ay[i][j] = matriz[i][j]

                detX = Det3x3(ax)
                detY = Det3x3(ay)
                detZ = Det3x3(az)  
                if determinante != 0:
                    if detX == 0:
                        x = 0
                    if detY == 0:
                        y = 0
                    if detZ == 0:
                        z = 0
                    if x != 0:
                        x = detX/determinante
                    if y != 0:
                        y = detY/determinante
                    if z != 0:
                        z = detZ/determinante
                
                    Mostrar(ax, 3, 3)
                    print('')
                    Mostrar(ay, 3, 3)
                    print('')
                    Mostrar(az, 3, 3)
                    print('\nDeterminante = {0}\nDetX = {1}\nDetY = {2}\nDetZ = {3}'.format(determinante,detX,detY,detZ))
                    print('\n----- RESULTADO SPD Sistema Linear 3x3 ------\nDeterminante {}\nX = {}\nY = {}\nZ = {}\n\n'.format(determinante,x,y,z))
                else:
                    if detX == 0 and detY == 0:
                        print('\n----- SPL -----\n\n')
                    if detX != 0 or detY != 0:
                        print('\n----- SI -----\n\n') 


            s2 = Opt3()

    s = Opt()

print("---------  ENCERRADO ---------\n")

